//Strict mode: variables must be declared before used
"use strict";
var globalVertexB=null;

function initVert()
{
    //1. define vertices
    //prostokat (xyz) 2d -> z=0
    /*var figuraXYZ = 
            [x1,y1,z1,
             x2,y2,z2,
             x3,y3,z3
             x4,y4,z4];*/
    var figuraXYZ = [
        0.5, 0.5, 0.0,
        -0.5, 0.5, 0.0,
        0.5, -0.5, 0.0,
        -0.5, -0.5, 0.0
    ];
         
    //2. create the buffer    
    globalVertexB = globalGL.createBuffer();
    
    //3. activate the buffer
    //void gl.bindBuffer(target, buffer);
    globalGL.bindBuffer(globalGL.ARRAY_BUFFER, globalVertexB);
    
    //4. Loading the buffer
    //void gl.bufferData(target, ArrayBufferView srcData, usage, srcOffset, length);
    globalGL.bufferData(globalGL.ARRAY_BUFFER, new Float32Array(figuraXYZ), globalGL.STATIC_DRAW);
}


