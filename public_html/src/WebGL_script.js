//Strict mode: variables must be declared before used
"use strict"; 
var globalGL = null;

function init()
{
    //init canvas
    var canvasGL=document.getElementById("GLCanvas");
    
    //init typ OpenGL
    globalGL=canvasGL.getContext("webgl") || canvasGL.getContext("experimental-webgl");
    
    
    //init kolor
    if (globalGL!==null)
    {
        // def szary kolor
        globalGL.clearColor(0.0, 0.8, 0.0, 1.0);
        //1. init the buffer
        initVert();
        
        //2. load shaders
        //function from the connector
        //parameters from the index (id)
        initShader("VertexShader", "FragmentShader");
        
    }
    else {
        document.write("<br><b>WebGL is not supported!</b>");
    }
}

// wymazacz w wyznaczonym kolorze
//function clear()
//{
//    globalGL.clear(globalGL.COLOR_BUFFER_BIT);
//}

//square
function DrawTest()
{
    //1. Take the color from the buffer:
    globalGL.clear(globalGL.COLOR_BUFFER_BIT);
    
    //2. Select the shader
    globalGL.useProgram(globalShader);
    
    //3. init the vertex
    globalGL.enableVertexAttribArray(globalVertexPosAttr);
    
    //4. Draw with special mode
    //void gl.drawArrays(mode, first, count);
    globalGL.drawArrays(globalGL.TRIANGLE_STRIP, 0, 4);
}


//rysowanie
function draw()
{
    init();
    //clear();
    DrawTest();
}